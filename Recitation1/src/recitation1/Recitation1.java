
package recitation1;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Yunshan Yan
 */
public class Recitation1 extends Application {
    
   public static void main(String[] args) {
        launch(args);
    }
    

    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        Button btn1= new Button();
        btn1.setText("say'goodbye crual world'");
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
       
        });
        btn1.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
       
        });
   
        
        StackPane root = new StackPane();

        root.getChildren().add(btn);
        root.getChildren().add(btn1);
        
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
     
}
}